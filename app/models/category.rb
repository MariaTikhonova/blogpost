class Category < ApplicationRecord
 
 has_many :posts

 validates :name, :length => {
    :minimum   => 2,
    :maximum   => 300,
    :tokenizer => lambda { |str| str.split.capitalize! },
    :too_short => "must have at least 2 words"
    }, format: { with: /[a-zA-Z_\-.]/,
    message: "only allows letters and ." }

end
