class Comment < ApplicationRecord
  belongs_to :post

     validates :author, presence: true, :length => {
    :minimum   => 2,
    :maximum   => 300,
    :tokenizer => lambda { |str| str.split.map(&:capitalize) },
    :too_short => "must have at least 2 words"
    }, format: { with: /[a-zA-Z_\-.]/,
    message: "only allows letters and ." }

    validates :content, presence: true
end
