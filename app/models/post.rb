require 'file_size_validator' 

class Post < ApplicationRecord

  belongs_to :category
  has_many :comments 
  mount_uploader :file, FileUploader

   validates :name, presence: true, :length => {
    :minimum   => 2,
    :maximum   => 300,
    :tokenizer => lambda { |str| str.split.map(&:capitalize) },
    :too_short => "must have at least 2 words"
    }, format: { with: /[a-zA-Z_\-.]/,
    message: "only allows letters and ." }

   validates :file, :file_size => { maximum: 2.megabytes.to_i  }
  
end
