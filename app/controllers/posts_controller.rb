class PostsController < ApplicationController
  skip_before_action :verify_authenticity_token
  before_action :set_post, only: [:show, :edit, :update, :destroy]
  before_action :set_category

  def index
    @posts = Post.all
  end

  def show
    @post = Post.find(params[:id])
    @comment = Comment.new
    @comment.post_id = @post.id
  end


  def new
    @post = Post.new
  end

  def edit
   @post = @category.posts.find(params[:id])
  end

  def create
    @post = @category.posts.build(post_params)
     @post.file = params[:file]
      if @post.save
       redirect_to category_url(@category)
      else
       render 'new'
      end
    
  end

  def update
    respond_to do |format|
      if @post.update(post_params)
        format.html { redirect_to category_post_url(@category, @post), notice: 'Post was successfully updated.' }
        format.json { render :show, status: :ok, location: @post }
      else
        format.html { render :edit }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @post.destroy
    respond_to do |format|
      format.html { redirect_to category_path(@category), notice: 'Post was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
   
    def set_post
      @post = Post.find(params[:id])
    end
    
    def set_category
      @category = Category.find(params[:category_id])
    end
   
    def post_params
      params.require(:post).permit(:name, :content, :file, :category_id)
    end
end
