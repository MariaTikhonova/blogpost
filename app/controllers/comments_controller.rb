class CommentsController < ApplicationController

 skip_before_action :verify_authenticity_token

	 def create
	@post = Post.find(params[:post_id])
 @comment = @post.comments.create(comment_params)
  respond_to do |format|
   format.js { @comment }
   format.html { redirect_to @post }
   format.json { render :comment, status: :created, location: @comment }
  end
end

def comment_params
 params.require(:comment).permit(:author, :content)
end

end
