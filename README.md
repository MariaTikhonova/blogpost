#Deployed at:
https://curiousthing.herokuapp.com/
![screenshot](app/assets/images/4.png)
![screenshot](app/assets/images/5.png)
![screenshot](app/assets/images/6.png)
Features:
* Rails 5.2
* Postgres on production
* bootstrap and jquery for frontend
* RSpec for tests
* Carrierwave for file uploading
