Rails.application.routes.draw do
  
  resources :categories do
    resources :posts
  end
  
  resources :posts do
  	 resources :comments
  end
  
  resources :posts

 	resources :comments



 root 'static_pages#home'
 get '/home', to: 'static_pages#home'
end
