require 'rails_helper'

RSpec.describe CommentsController, type: :controller do


  let(:valid_attributes) {
    skip(author: "Username Usersurname", content: "texts")
  }

  let(:invalid_attributes) {
    skip(author: nil, content: nil)
  }

  let(:valid_session) { {} }

  describe "POST #create" do
    context "with valid params" do
      it "creates a new Comment" do
        expect {
          post :create, params: {comment: valid_attributes}, session: valid_session
        }.to change(Comment, :count).by(1)
      end

      it "adds without redirect" do
        post :create, params: {post: valid_attributes}, session: valid_session
        expect(response).to be_successful
      end
    end

    context "with invalid params" do
      it "shows comment form" do
        post :create, params: {comment: invalid_attributes}, session: valid_session
        expect(response).to be_successful
      end
    end
  end

end
