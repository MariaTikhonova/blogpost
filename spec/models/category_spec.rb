require 'rails_helper'

RSpec.describe Category, type: :model do

   before do
    subject { described_class.new(:name => "User Says", :description => "These include tasks like
checking out the code from a git repository") }
  end

  describe "ActiveModel validations" do

  it "should not be valid without name" do
  	subject.name = nil
  	expect(subject).not_to be_valid
   expect(subject).to validate_presence_of(:name)
   end

  it "should validate name's length" do
  	expect(subject.name.split.length).should be > 2
   expect(subject.name.split.each.size).should be > 2
  end

  it "should validate name's format" do
  	 expect(subject.name.split.each).to eq(subject.name.split.each.capitalize!)
  end
 
 end 

 describe "ActiveRecord associations" do

  it "should have posts" do
    expect(subject).to have_many(:comments)
  end 

 end

end
