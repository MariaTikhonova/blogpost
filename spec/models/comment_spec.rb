require 'rails_helper'

RSpec.describe Comment, type: :model do
  before(:each) do
 	subject { Comment.new{author: "Username Usersurname", content: "texts"} }
  end

  describe "ActiveModel Validations" do
   
   it "should have valid attributes" do
   	expect(subject).to validate_presence_of(:author)
   	expect(subject).to validate_presence_of(:content)
   	expect(subject.author).to be_valid
   end

   it "should validate format" do
   	expect(subject.author.split.length).should be > 2
    expect(subject.author.split.each.size).should be > 2
    expect(subject.author.split.each).to eq(subject.author.split.each.upcase!)
   end

  end

  describe "ActiveRecord relations" do

  	it "should belong to post" do
  	 expect(subject).to belong_to(:post)
  	end

  end
end
