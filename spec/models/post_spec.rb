require 'rails_helper'

RSpec.describe Post, type: :model do

  before do
    subject { described_class.new(:name => "User Says", :content => "Capistrano is a ruby gem which provides a framework for automating tasks related to deploying
a ruby based application, in our case a Rails app, to a remote server.", :file => "image.jpg") }
  end

  describe "ActiveModel validations" do

  it "should not be valid without name" do
  	subject.name = nil
  	expect(subject).not_to be_valid
    expect(subject).to validate_presence_of(:name)
  end

  it "should validate name's length" do
  	expect(subject.name.split.length).should be > 2
   expect(subject.name.split.each.size).should be > 2
  end

  it "should validate name's format" do
  	 expect(subject.name.split.each).to eq(subject.name.split.each.capitalize!)
  end

  it "should validate file's size" do
 	  expect(subject.file.size.to_i).should be < 2
  end
 
 end 

 describe "ActiveRecord associations" do

  it "should have comments" do
    expect(subject).to have_many(:comments)
  end    

  it "should belong to category" do
    expect(subject).to belong_to(:categories)
  end 
  
 end

end
