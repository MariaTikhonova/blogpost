require 'carrierwave/test/matchers'

describe FileUploader do
  include CarrierWave::Test::Matchers

  let(:post) { double('post') }
  let(:uploader) { FileUploader.new(post, :file) }

  before do
    MyUploader.enable_processing = true
    File.open(path_to_file) { |f| uploader.store!(f) }
  end

  after do
    MyUploader.enable_processing = false
    uploader.remove!
  end

  context 'is not larger than' do
    it "doesn't upload larger files" do
      expect(uploader.file).to be_no_larger_than(2)
    end
  end

  it "has the correct format" do
    expect(uploader).to be_format %w(jpg jpeg gif png pdf docx mp3)
  end
end